package files;

import java.io.File;
import java.io.IOException;

public class FileOperations1 {
    public static void main(String[] args) throws IOException {
        File f1 = new File("C:\\Users\\Aijaz\\Documents\\files");
        System.out.println("File name : " + f1.getName());
        System.out.println("Is directory : " + f1.isDirectory());
        System.out.println("" + f1.listFiles().length);
        System.out.println("" + f1.toString());
        System.out.println("" + f1.getAbsolutePath());
        System.out.println("" + f1.getCanonicalPath());
        System.out.println("" + f1.getParent());
        System.out.println("" + f1.getPath());
        System.out.println("" + f1.exists());
        System.out.println("" + f1.isFile());
        System.out.println("" + f1.mkdirs());



    }
}
