package files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class moveFldr {
    public static void main(String[] args) throws IOException {
        Path Src = Paths.get("C:\\Users\\Aijaz\\Documents\\files\\fld1");
        Path Dest = Paths.get("C:\\Users\\Aijaz\\Documents\\files\\fld1copy");
//        Files.copy(Src,Dest);


        Files.copy(Src, Dest);
    }
    static void dir(File folder){
        System.out.println(folder.getName()+" :");
        File[] fil1=folder.listFiles();
        for (File f:fil1){
            if(f.isDirectory())
                dir(f.getAbsoluteFile());
            else
                System.out.println("\t "+f.getName());

        }

    }
}