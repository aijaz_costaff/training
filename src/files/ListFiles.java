package files;

import java.io.File;

public class ListFiles {
    public static void main(String[] args) {
        File f1 = new File("C:\\Users\\Aijaz\\Documents\\files");

        System.out.println("*******------- Start of reading folder '"+f1.getName()+"' -------*******");
        dir(f1);
        System.out.println("*******------- End of reading folder '"+f1.getName()+"' -------*******");
    }
    static void dir(File folder){
        System.out.println(folder.getName()+" :");
        File[] fil1=folder.listFiles();
        for (File f:fil1){
            if(f.isDirectory())
                dir(f.getAbsoluteFile());
            else
                System.out.println("\t "+f.getName());

        }

    }
}
