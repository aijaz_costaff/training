package files;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerialDemo {
    public static void main(String [] args) {
        Employee e = new Employee();
        e.name = "Aijaz Ahmad";
        e.address = "Ganderbal, Kashmir";
        e.SSN =232 ;
        e.pin = 101;

        try {
            FileOutputStream fileOut =
                    new FileOutputStream("C:\\Users\\Aijaz\\Documents\\files\\employee.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut); ///
            out.writeObject(e);
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved ");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }
}
