package files.readWrite;

import java.io.*;

public class BufferdWrite {

    public static void main(String[] args) {
        long start = 0;
        long end = 0;
        long tfis=0;
        long tbis=0;
        File f2=new File("C:\\Users\\Aijaz\\Documents\\files\\fld1\\fld2\\t2.txt");
        File f4=new File("C:\\Users\\Aijaz\\Documents\\files\\fld1\\fld2\\t4.txt");
        File f2b=new File("C:\\Users\\Aijaz\\Documents\\files\\fld1\\fld2\\t2b.txt");
        File f2a=new File("C:\\Users\\Aijaz\\Documents\\files\\fld1\\fld2\\t2a.txt");
        try {
            FileOutputStream fos =new FileOutputStream(f2a);
            BufferedOutputStream bos=new BufferedOutputStream(new FileOutputStream(f2b));
            FileInputStream fis=new FileInputStream(f2);
            BufferedInputStream bis=new BufferedInputStream(new FileInputStream(f4) );
            System.out.println("Writing file with FileStream ..... \n");
            start = System.nanoTime();
            while (fis.available() > 0)
                fos.write(fis.read());
            end = System.nanoTime();
            tfis=end-start;
            System.out.println(" \n Time taken by FileStream = "+tfis);

            System.out.println("\n-----------------------------------------------------------------------------------------");
            System.out.println("Writing file with BufferdStream :");
            System.out.println("-----------------------------------------------------------------------------------------\n");
            start = System.nanoTime();
            while (bis.available() > 0)
                bos.write(bis.read());
            bos.flush();
            end = System.nanoTime();
            tbis=end-start;
            System.out.println(" \n Time taken by BufferdStream = "+tbis);
            System.out.println("\n-----------------------------------------------------------------------------------------");
            System.out.println("Difference = FOS - BOS => "+(tfis-tbis));
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }


    }

}
