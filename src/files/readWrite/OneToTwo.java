package files.readWrite;

import java.io.*;

public class OneToTwo {
    public static void main(String[] args) throws IOException {
        File inf= new File("Files/input.txt");
        FileInputStream fis=new FileInputStream(inf);
        System.out.println(fis.available());
       File o1=new File("Files/one.txt");
        File o2=new File("Files/two.txt");
        System.out.println("Creating file 1");

        o1.createNewFile();
        System.out.println("Creating file 2");
        o2.createNewFile();
        FileOutputStream  fos1=new FileOutputStream(o1);
        FileOutputStream  fos2=new FileOutputStream(o2);
        System.out.println("Writing files ");

        while (fis.available() > 0){
            int b=fis.read();
            fos1.write(b);

        }
//        fis.mark();
        System.out.println(fis.available());
        while (fis.available() > 0){
            int b=fis.read();

            fos2.write(b);
        }

        System.out.println("Completed");
    }
}
