package files.readWrite;

import java.io.*;

public class BufferdDemo {
    public static void main(String[] args) {
        long start = 0;
        long end = 0;
        long tfis=0;
        long tbis=0;
        File f2=new File("C:\\Users\\Aijaz\\Documents\\files\\fld1\\fld2\\t2.txt");
        try {
//            FileOutputStream fos =new FileOutputStream(f2,true);
            FileInputStream fis=new FileInputStream(f2);
            BufferedInputStream bis=new BufferedInputStream(new FileInputStream(f2) );
            System.out.println("Reading file with FileInputStream ..... \n");
            start = System.nanoTime();
            while (fis.available() > 0)
                System.out.print((char)fis.read());
            end = System.nanoTime();
            tfis=end-start;
            System.out.println(" \n Time taken by FileInputStream = "+tfis);

            System.out.println("\n-----------------------------------------------------------------------------------------");
            System.out.println("Reading file with BufferdInputStream :");
            System.out.println("-----------------------------------------------------------------------------------------\n");
            start = System.nanoTime();
            while (bis.available() > 0)
                System.out.print((char)bis.read());
            end = System.nanoTime();
            tbis=end-start;
            System.out.println(" \n Time taken by BufferdInputStream = "+tbis);
            System.out.println("\n-----------------------------------------------------------------------------------------");
            System.out.println("Difference = FIS - BIS => "+(tfis-tbis));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }


    }
}
