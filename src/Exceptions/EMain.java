package Exceptions;

public class EMain {
    public static void main(String[] args)  {
        int i=20;
        int b=1;
        String s1=null;
        String s2=null;

        try {
            System.out.println(i/b);
            System.out.println(s1.equals(s2));
            throw new ExThrowable("Error occured dear");
        }
        catch (ArithmeticException ae){
            System.out.println("there is an Arithmetic exception in your program");
        }
        catch (Exception | ExThrowable e){
            System.out.println(e.getMessage());

        }
        finally {
            System.out.println("Finally block");
        }
        System.out.println("After exception");
    }
}
