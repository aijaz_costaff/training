package Exceptions;

public class ExThrowable extends Throwable{
    ExThrowable(String msg){
        super(msg);
    }
}
class ExRuntime extends RuntimeException{
    ExRuntime(String msg){
        super(msg);
    }
}
