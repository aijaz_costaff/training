package Exceptions;

public class Exceptions {
    public static void main(String[] args) {

        try {
            int x=79;
            if(! (x%2 == 0))
                throw new ExRuntime("runtime error");
            System.out.println("no exception");

        }
        catch (ExRuntime er){
            System.out.println(er.getMessage());
        }
        System.out.println("after exception");
    }
}
