package SerializationDemo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerializeDemo {
    public static void main(String[] args) {
        Student obj=new Student();
        obj.setAge(26);
        obj.setName("Aijaz");
        Address ad =new Address();
        ad.city="Srinagar";
        ad.locality="Ganderbal";
        ad.pin=191201;

        obj.setAdderss(ad);
        try (
                FileOutputStream fos=new FileOutputStream("objj.txt");
                ObjectOutputStream os=new ObjectOutputStream(fos);
                ) {
            os.writeObject(obj);
            System.out.println("object serialized ");

        }
        catch (Exception e){
            System.err.println("Unable to Serialize");

        }

    }
}
