package SerializationDemo;

import java.io.Serializable;

public class Student implements Serializable {
    static int id;
    private String name;
    public String stream;
    protected int age;
    transient long cell;
    Address adderss;

    public Address getAdderss() {
        return adderss;
    }

    public void setAdderss(Address adderss) {
        this.adderss = adderss;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Student.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", stream='" + stream + '\'' +
                ", age=" + age +
                ", adderss=" + adderss +
                '}';
    }
}
class Address implements Serializable{
    String city;
    String locality;
    int pin;

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                ", locality='" + locality + '\'' +
                ", pin=" + pin +
                '}';
    }
}