package miniProject;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

public class funs {
    //Store
    public void store(Set<Employee> e) {
        Set<Employee> sobj = new LinkedHashSet<Employee>(e);
        try (FileOutputStream fos = new FileOutputStream("objj.txt");
             ObjectOutputStream os = new ObjectOutputStream(fos);
        ) {
            os.writeObject(sobj);
            System.out.println("object Saved permenently ");

        } catch (Exception er) {
            System.err.println("Unable to Store");

        }

    }
    //load

    public void load(Set<Employee> e) {
        try (FileInputStream fis = new FileInputStream("objj.txt");
             ObjectInputStream ois = new ObjectInputStream(fis);) {
            Object oo = ois.readObject();
            register.set = (LinkedHashSet<Employee>) oo;
        } catch (Exception er) {
            er.printStackTrace();
            System.err.println("Error no records found ");

        }

    }

    // INSERT Function
    public void insertd(Set<dept> e) {
        Scanner sc = new Scanner(System.in);
        dept eeobj = new dept();
        while (true) {
            System.out.println("Enter Department id :");
            eeobj.setDept_id(sc.nextInt());
            System.out.println("Enter Department name :");
            eeobj.setDept_name(sc.next());
            System.out.println("Enter Department Address :");
            eeobj.setLocation(sc.next());
            e.add(eeobj);
            System.out.println("Added Successfully");
            System.out.println("Do you want to add another Department  'y' for yes:");
            if (sc.next().charAt(0) != 'y')
                break;
            eeobj = new dept();
        }

    }

    // INSERT Function
    public void insert(Set<Employee> e) {
        Scanner sc = new Scanner(System.in);
        Employee eeobj = new Employee();
        while (true) {
            System.out.println("Enter Employee id :");
            eeobj.setId(sc.nextInt());
            System.out.println("Enter Employee name :");
            eeobj.setName(sc.next());
            System.out.println("Enter Employee Salary");
            eeobj.setSalary(sc.nextFloat());
            System.out.println("Enter Employee Address :");
            eeobj.setAddress(sc.next());
            System.out.println("Enter Employee Department id :");
            eeobj.setDept_id(sc.nextInt());
            e.add(eeobj);
            System.out.println("Added Successfully");
            System.out.println("Do you want to add another Employee  'y' for yes:");
            if (sc.next().charAt(0) != 'y')
                break;
            eeobj = new Employee();
        }

    }

    // Delete Function
    public void delete(Set<Employee> e) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter id of Employee to delete");
        int id = sc.nextInt();
        Iterator<Employee> ite = e.iterator();
        while (ite.hasNext()) {
            Employee obj = ite.next();
            if (obj.getId() == id) {
                e.remove(obj);
                break;
            }
            System.out.println("Employee removed Successfully");
        }
    }

    // Search Function
    public void search(Set<Employee> e) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter id of Employee to search");

        int id = sc.nextInt();
        Iterator<Employee> ite = e.iterator();
        while (ite.hasNext()) {
            Employee obj = ite.next();
            if (obj.getId() == id) {
                System.out.println(" \n");
                System.out.println("Emp_ID \t Emp_Name \t Address \t Salary \t Dep_ID");
                System.out.println(obj.getId() + " \t " + obj.getName() + " \t " + obj.getAddress() + " \t " + obj.getSalary() + " \t " + obj.getDept_id());

            }
        }

    }

    // List Function
    public void list(Set<Employee> e) {
        System.out.println(" \n");
        System.out.println("Emp_ID  \t Emp_Name \t Address \t  Salary \t   Dep_ID");
        Iterator<Employee> itr = e.iterator();
        while (itr.hasNext()) {
            Employee eep = itr.next();
            System.out.println(eep.getId() + " \t      " + eep.getName() + " \t    " + eep.getAddress() + " \t      " + eep.getSalary() + " \t   " + eep.getDept_id());
        }


    }

    // sorting
    public void sort(Set<Employee> e) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Choose from options to sort :\n    'i' by ID , 'n' by name ,'s' by salary ,'d' by department_ID ");
        final char op = sc.next().charAt(0);
        switch (op) {
            case 'i':
                // create ArrayList and store HashSet contents
                List<Employee> elist = new ArrayList(e);
                Collections.sort(elist, new idcomprator());
                register.set = new LinkedHashSet<>(elist);
                break;
            case 'n':
                List<Employee> elist1 = new ArrayList(e);
                Collections.sort(elist1, new namecomprator());
                register.set = new LinkedHashSet<>(elist1);
                break;
            case 's':
                List<Employee> elist2 = new ArrayList(e);
                Collections.sort(elist2, new salarycomprator());
                register.set = new LinkedHashSet<>(elist2);
                break;
            case 'd':
                List<Employee> elist3 = new ArrayList(e);
                Collections.sort(elist3, new deptcomprator());
                register.set = new LinkedHashSet<>(elist3);
                break;

            default:
                System.out.println("Invalid Sorting Option ");
        }


    }

    // update Function
    public void update(Set<Employee> e) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter id of Employee to delete");
        int id = sc.nextInt();
        Iterator<Employee> ite = e.iterator();
        while (ite.hasNext()) {
            Employee obj = ite.next();
            if (obj.getId() == id) {
                e.remove(obj);
                this.insert(e);
                break;
            }
            System.out.println("Employee Updated ");
        }

    }
}
// comprator


class idcomprator implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
        if (o1.getId() - o2.getId() == 0)
            return 0;
        else if (o1.getId() - o2.getId() > 0)
            return 1;
        else
            return -1;
    }

    @Override
    public Comparator<Employee> reversed() {
        return Comparator.super.reversed();
    }
}

class namecomprator implements Comparator<Employee> {
    @Override
    public int compare(Employee o1, Employee o2) {
        return o1.getName().compareTo(o2.getName());
    }

}

class deptcomprator implements Comparator<Employee> {
    @Override
    public int compare(Employee o1, Employee o2) {
        if (o1.getDept_id() - o2.getDept_id() == 0)
            return 0;
        else if (o1.getDept_id() - o2.getDept_id() > 0)
            return 1;
        else
            return -1;
    }
}

class salarycomprator implements Comparator<Employee> {
    @Override
    public int compare(Employee o1, Employee o2) {
        if (o1.getSalary() - o2.getSalary() == 0)
            return 0;
        else if (o1.getSalary() - o2.getSalary() > 0)
            return 1;
        else
            return -1;
    }

}

