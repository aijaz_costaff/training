package miniProject;

import java.io.Serializable;
import java.util.*;

public class register {
    static Set<Employee> set = new LinkedHashSet<>();
    static Set<dept> setd = new LinkedHashSet<>();
    static funs fobj = new funs();

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Do you Want to load Your Stored Data. 'y' for yes , 'n' for no :");
        char op = scan.next().charAt(0);
        if (op == 'y')
            fobj.load(set);
        while (true) {
            System.out.println("  ");
            System.out.println("Enter your option :  'i' for insert , 'u' for update , 'd' for delete , 'l' for list ,'s' for search \n " +
                    ",press 'c' to sort ,press '1' to insert department , press 'q' to exit");
            try {
                op = scan.next().charAt(0);
                if (op == 'q') {
                    System.out.println("Do you want to save your work. 'y' for yes , 'n' for no ");
                    op = scan.next().charAt(0);
                    if (op == 'y') {
                        fobj.store(set);
                    }
                    break;
                }

                options(op);

            } catch (Exception e) {
                System.out.println("operation Failed . Please Try again .");
                continue;
            }


        }

    }


    static void options(char op) {
        final char option = op;
        switch (option) {
            case 'i':
                fobj.insert(set);
                break;
            case 'u':
                fobj.update(set);
                break;
            case 'd':
                fobj.delete(set);
                break;
            case 's':
                fobj.search(set);
                break;
            case 'l':
                fobj.list(set);
                break;
            case 'c':
                fobj.sort(set);
                break;

            default:
                System.out.println("Invalid choice");
                break;
        }

    }


}

class Employee implements Serializable {
    int id;
    String name;
    float salary;
    String address;

    public Employee() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getDept_id() {
        return dept_id;
    }

    public void setDept_id(int dept_id) {
        this.dept_id = dept_id;
    }

    int dept_id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", address='" + address + '\'' +
                ", dept_id=" + dept_id +
                '}';
    }

    public Employee(int id, String name, float salary, String address, int dept_id) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.address = address;
        this.dept_id = dept_id;
    }
}

class dept {
    int dept_id;
    String dept_name;
    String location;

    public dept(int dept_id, String dept_name, String location) {
        this.dept_id = dept_id;
        this.dept_name = dept_name;
        this.location = location;
    }

    public dept() {

    }

    public int getDept_id() {
        return dept_id;
    }

    public void setDept_id(int dept_id) {
        this.dept_id = dept_id;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        dept dept = (dept) o;
        return dept_id == dept.dept_id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(dept_id);
    }

    @Override
    public String toString() {
        return "dept{" +
                "dept_id=" + dept_id +
                ", dept_name='" + dept_name + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
