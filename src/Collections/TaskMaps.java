package Collections;

import java.util.*;

public class TaskMaps {
    public static void main(String[] args) {
        Map map=new HashMap();
        List<Employee> elist=new ArrayList<Employee>();
       for (int i=1;i<21;i++)
           elist.add(new Employee(100+i,"Aijaz"+1,20+i,201));

       dept dep=new dept(201,"IT","Banglore");
       map.put(dep,elist);
        System.out.println(map.values());
        System.out.println(map.keySet());
    }

}
class Employee{
    int id;
    String name;
    int age;
    int dept_id;

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", dept_id=" + dept_id +
                '}';
    }

    public Employee(int id, String name, int age, int dept_id) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.dept_id = dept_id;
    }
}
class dept{
    int dept_id;
    String dept_name;
    String location;

    public dept(int dept_id, String dept_name, String location) {
        this.dept_id = dept_id;
        this.dept_name = dept_name;
        this.location = location;
    }

    @Override
    public String toString() {
        return "dept{" +
                "dept_id=" + dept_id +
                ", dept_name='" + dept_name + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
