package Collections;

import java.util.*;

public class demo2 {
    public static void main(String[] args) {
        List list1=new ArrayList();
        Set set1= new HashSet();
        Set set2 = new TreeSet();

        for(int i=0;i<9;i++){
            list1.add("item"+i);
            set1.add("item"+i);
            set2.add("item"+i);
        }
        Iterator it1=list1.iterator();
        Iterator it2 = set2.iterator();
        Iterator it3 = set1.iterator();
        System.out.println("\n-----------------------------------------------------------------------------------\n");

        while (it1.hasNext())
        {
            System.out.println(it1.next());

        }
        System.out.println("\n-----------------------------------------------------------------------------------\n");
        while (it3.hasNext())
        {

            System.out.println(it3.next());
        }
        System.out.println("\n-----------------------------------------------------------------------------------\n");

        while (it2.hasNext())
        {
            System.out.println(it2.next());

        }

    }

}
