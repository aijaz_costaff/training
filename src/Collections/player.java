package Collections;

import java.util.Comparator;
import java.util.Objects;

public class player {
    String name;
    int score;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        player player = (player) o;
        return score == player.score && name.equals(player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, score);
    }
}
class Scorecomprator implements Comparator<player>{

    @Override
    public int compare(player o1, player o2) {
        return o2.score-o1.score;
    }

}
class Namecomprator implements Comparator<player>{
    @Override
    public int compare(player o1, player o2) {
        return o2.name.compareTo(o1.name);
    }
}

