package lucky;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class EqualsTest {
    public static void main(String[] args) {
        Set<Emp1> set=new HashSet<>();
        set.add(new Emp1(121,"Soumya",222));










        System.out.println(set.size());
    }
}
class Emp1{
    int empid;
    String empname;
    int depid;

    @Override
    public String toString() {
        return "Emp1{" +
                "empid=" + empid +
                ", empname='" + empname + '\'' +
                ", depid=" + depid +
                '}';
    }

    public Emp1(int empid, String empname, int depid) {
        this.empid = empid;
        this.empname = empname;
        this.depid = depid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Emp1 emp1 = (Emp1) o;
        return empid == emp1.empid && depid == emp1.depid && empname.equals(emp1.empname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(empid, empname, depid);
    }
}
