package lucky;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class maptest {
    public static void main(String[] args) {

        Dept dobj = new Dept(222,"Testing","Banglore");
        List<Emp> empList = new ArrayList<>();
        empList.add(new Emp(111,"Soumya",222));
        empList.add(new Emp(111,"Soumya",222));
        empList.add(new Emp(111,"Soumya",222));
        empList.add(new Emp(111,"Soumya",222));
        Map<Dept,List<Emp>> map=new HashMap<>();
        map.put(dobj,empList);
        System.out.println(map.keySet());
        System.out.println(map.values());
    }
}
class Emp{
    int empid;
    String empname;
    int depid;

    @Override
    public String toString() {
        return "Emp{" +
                "empid=" + empid +
                ", empname='" + empname + '\'' +
                ", depid=" + depid +
                '}';
    }

    public Emp(int empid, String empname, int depid) {
        this.empid = empid;
        this.empname = empname;
        this.depid = depid;
    }
}
class Dept{
    int depid;
    String Depname;
    String location;

    public Dept(int depid, String depname, String location) {
        this.depid = depid;
        Depname = depname;
        this.location = location;
    }

    @Override
    public String toString() {
        return "Dept{" +
                "depid=" + depid +
                ", Depname='" + Depname + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}