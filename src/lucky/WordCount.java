package lucky;

import java.io.*;
import java.util.*;

public class WordCount {


    public static void main(String[] args) throws IOException {
        String line="";
        List<String> list=new ArrayList();
        Set<String> uniq=new HashSet<>();
        FileReader file=new FileReader("demo.txt");
        BufferedReader br=new BufferedReader(file);
        while ((line=br.readLine()) != null){
            line=line.toLowerCase(Locale.ROOT);
            String[] words=line.split(" ");
            for (String word:words){
                uniq.add(word);
                list.add(word);
            }

        }
        for (String uw:uniq){
            int count=0;
            for (String lw:list)
            {
                if (uw.equals(lw))
                    count++;
            }
            System.out.println(uw+" appeared "+count+" Times");
        }


    }


}
