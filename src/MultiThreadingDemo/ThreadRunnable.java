package MultiThreadingDemo;

public class ThreadRunnable {
    public static void main(String[] args) {
        RThread1 rt=new RThread1();
        Thread t1=new Thread(rt);
        t1.start();
        System.out.println(t1.getName());

    }
}
class RThread1 implements Runnable{

    @Override
    public void run() {
        for (int i=0;i<20;i++){
            System.out.println("thread-1 running : ");
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
