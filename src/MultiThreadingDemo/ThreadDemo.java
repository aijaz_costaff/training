package MultiThreadingDemo;

public class ThreadDemo {
    public static void main(String[] args) throws InterruptedException {

        Thread1 t1 = new Thread1();
        Thread t2 = new Thread2();
        Thread t3 = new Thread3();

//            t2.setPriority(1);

        System.out.println(Thread.currentThread().isAlive());

        System.out.println(t1.getPriority());

        t1.join();
        t2.join();
        t3.join();
        t1.start();
        t2.start();
        t3.start();
//            t4.join();

//        Thread.currentThread().join();
        System.out.println("===================================================================-------------------------end");

    }
}

class Thread1 extends Thread {
    public void run() {
        Thread t4 = new Thread4();
        t4.start();
        for (int i = 0; i < 20; i++) {
            System.out.println("thread-1 running : " + this.getPriority());
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }
//   public void start(){
//       System.out.println("------------------------------------");
//   }
}

class Thread2 extends Thread {
    public void run() {
        for (int i = 0; i < 20; i++)
            System.out.println("thread-2 running : " + this.getPriority());
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}

class Thread3 extends Thread {
    public void run() {
        for (int i = 0; i < 20; i++)
            System.out.println("thread-3 running : " + this.getPriority());
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}

class Thread4 extends Thread {
    public void run() {
        for (int i = 0; i < 20; i++)
            System.out.println("thread-4 running : " + this.getPriority());

    }
}