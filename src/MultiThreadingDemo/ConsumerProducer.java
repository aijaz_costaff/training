package MultiThreadingDemo;

import java.util.ArrayList;
import java.util.List;

public class ConsumerProducer {
    public static void main(String[] args) {
        List<Integer> box=new ArrayList<>();
        Producer producer=new Producer(box);
        Consumer consumer=new Consumer(box);
        producer.start();
        consumer.start();


    }
}
class Producer extends Thread{
    List<Integer> box;
    int MAX_SIZE=5;
    int item=0;

    public Producer(List<Integer> box){this.box=box;}
    public void run(){
       while (true) {
           produce(item++);

       }



    }
    public void produce(Integer data){
        synchronized (box){
            while (box.size()>= MAX_SIZE){
                System.out.println("Box full waiting for consumer");
                try {
                    box.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        synchronized (box){
            box.add(data);
            System.out.println("producer produced "+ data);
            box.notify();
        }

    }


}
class Consumer extends Thread{


    List<Integer> box;
    int MAX_SIZE=5;
    int item=0;
    int run=4;

    public Consumer(List<Integer> box){this.box=box;}
    public void run(){
        while ((true))
            consume();



    }
    public void consume(){
        synchronized (box){
            while (box.size() <= 0){
                System.out.println("Box Empty Waiting for Producer");
                try {
                    box.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }        }
        synchronized (box){
            System.out.println("item Consumed "+ box.remove(0));
            box.notify();
            
        }

    }




}
