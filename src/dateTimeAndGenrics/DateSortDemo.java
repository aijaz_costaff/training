package dateTimeAndGenrics;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateSortDemo {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat df=new SimpleDateFormat("dd/MM/yyy");
//
        LinkedHashSet<Employee> eset=new LinkedHashSet();
        eset.add(new Employee(103,"Saleem",df.parse("10/11/1999")));
        eset.add(new Employee(102,"Aijaz",df.parse("10/01/2021")));
        eset.add(new Employee(104,"Zahid",df.parse("10/11/1995")));
        eset.add(new Employee(101,"Naseer",df.parse("10/05/1990")));
        ArrayList<Employee> elist=new ArrayList(eset);
        System.out.println("Before Sorting");
        for (Employee e:eset)
            System.out.println(e);
        Collections.sort(elist,new DateComparetor());
        eset=new LinkedHashSet<>(elist);
        System.out.println("After Sorting");
        for (Employee e:eset)
            System.out.println(e);
        elist=new ArrayList(eset);
        Collections.sort(elist,new IdComparetor());
        eset=new LinkedHashSet<>(elist);
        System.out.println("After Sorting by ID");
        for (Employee e:eset)
            System.out.println(e);
    }
}
class Employee {
    int id;
    String name;
    Date joining;

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", joining=" + joining +
                '}';
    }

    public Employee(int id, String name, Date joining) {
        this.id = id;
        this.name = name;
        this.joining = joining;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getJoining() {
        return joining;
    }

    public void setJoining(Date joining) {
        this.joining = joining;
    }
}
class DateComparetor implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
        return o1.getJoining().compareTo(o2.getJoining());
    }


}
class IdComparetor implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
        return o1.getId()-o2.getId();
    }


}

