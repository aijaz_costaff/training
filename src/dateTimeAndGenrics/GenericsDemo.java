package dateTimeAndGenrics;

import java.util.Date;

public class GenericsDemo {
    public static void main(String args[]){
        MyGen<Integer> m=new MyGen<Integer>();
        MyGen<String> m1=new MyGen();
        m1.setObj("hello");
        System.out.println(m1.getObj());
        System.out.println(m1.getObj().getClass());
        m.setObj(2);
//m.add("vivek");//Compile time error
        System.out.println(m.getObj());
        System.out.println(m.getObj().getClass());
        getType(new Date());
    }
    static <E>void getType(E T){
        System.out.println(T.getClass());

    }
}
class MyGen<T>{
    T obj;

    public T getObj() {
        return obj;
    }

    public void setObj(T obj) {
        this.obj = obj;
    }
}