package annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@annote(value1 = 1,value2 = "v2",value3 = "v3")
public class AnnotationsDemo {
    @annote()
    System s;
    void printe(){
        @annote
        String ss;
    }

    public static void main(String[] args) {

    }
}
@Target({ElementType.TYPE,ElementType.FIELD,ElementType.LOCAL_VARIABLE})
@Retention(RetentionPolicy.RUNTIME)
@interface annote{
    int value1() default 1;
    String value2() default "";
    String value3() default "xyz";
}
