package annotations;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpCall {
    public static void main(String[] args) {
        try {

            URL url = new URL("https://x.wazirx.com/wazirx-falcon/api/v2.0/crypto_rates");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
//            System.out.println( conn.getResponseMessage().getClass());



            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            JSONParser parser = new JSONParser();

            JSONObject json =null;
            while ((output = br.readLine()) != null) {
//                System.out.println(output);
                json = (JSONObject) parser.parse(output);
            }

            System.out.println(json.keySet());
            System.out.println(json.values());
            conn.disconnect();

        } catch (Exception e) {
           e.printStackTrace();
        }
    }
}

