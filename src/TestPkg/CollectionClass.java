package TestPkg;

import java.util.*;

public class CollectionClass {


    public static void main(String[] args) {
        List <Student> list=new ArrayList<>();
        for(int i=1;i<20;i++){
            list.add(new Student(i,"student"+i,500*i));
            list.add(new Student(i,"student"+i,500*i));

        }
        Set<Student> lst=new HashSet<Student>(list);
        System.out.println(lst.size());

        for(Student obj:lst)
            System.out.println(obj);


    }
}
class Student{
    int id;
    String name;
    int salary;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                '}';
    }

    public Student(int id, String name, int salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
class idcomprator implements Comparator<Student> {



    @Override
    public int compare(Student o1, Student o2) {
        return o1.id-o2.id;
    }
}
